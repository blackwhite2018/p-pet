import React, { HTMLAttributes } from 'react';

interface IWrapperProps extends HTMLAttributes<HTMLDivElement> {}

export const Wrapper: React.FC<IWrapperProps> = ({ children, ...rest }) => <div {...rest}>{children}</div>;
