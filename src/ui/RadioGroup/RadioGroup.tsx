import React from 'react';
import cn from 'classnames';
import classes from './radio-group.module.scss';
import { Wrapper } from './Wrapper';

interface IRadioGroupContext {
  name?: string;
  onChange?: React.FormEventHandler<HTMLElement>;
  selectedValue?: number | string;
}

const Context = React.createContext<IRadioGroupContext>({});

export { Context };

enum EDirection {
  horizontal = 'horizontal',
  vertical = 'vertical',
}

interface IRadioGroupProps extends React.HTMLProps<HTMLElement> {
  direction?: string;
  selectedValue?: number | string;
}

export const RadioGroup: React.FC<IRadioGroupProps> = ({
  children,
  direction = EDirection.horizontal,
  className,
  name,
  onChange,
  selectedValue = 0,
  ...rest
}) => {
  return (
    <Context.Provider value={{ name, onChange, selectedValue }}>
      <Wrapper
        {...rest}
        className={cn(className, {
          [classes.vertical]: direction === 'vertical',
        })}
      >
        {children}
      </Wrapper>
    </Context.Provider>
  );
};
