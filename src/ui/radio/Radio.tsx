import React, { useContext } from 'react';
import cn from 'classnames';
import classes from './radio.module.scss';
import { Context } from '../RadioGroup/RadioGroup';

interface IRadioProps extends React.HTMLProps<HTMLInputElement> {}

export const Radio: React.FC<IRadioProps> = ({ children, className, id, ...rest }) => {
  const { selectedValue, onChange, name } = useContext(Context);
  return (
    <label
      htmlFor={id}
      className={cn(
        classes['radio-button'],
        className,
        {
          [classes['active-radio-button']]: String(selectedValue) === String(rest.value),
        },
        { [classes['disabled-radio-button']]: rest.disabled },
      )}
    >
      <div className={classes['radio-input']} />
      <input {...rest} name={name || rest.name} onClick={onChange} className={classes.input} type="radio" id={id} />
      {children}
    </label>
  );
};
